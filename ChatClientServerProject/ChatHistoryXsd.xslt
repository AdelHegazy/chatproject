<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/chatHistory/chat[1]">
	<br/>
		<xsl:apply-templates select="to"/>
		<br/>
	</xsl:template>
	
	<xsl:template match="/chatHistory/chat[1]">
	<br/>
		<xsl:apply-templates select="from"/>
		<br/>
	</xsl:template>
	
	<xsl:template match="/chatHistory/chat[1]">
	<br/>
		<xsl:apply-templates select="time"/>
		<br/>
	</xsl:template>
	
	
	
	<xsl:template match="/chatHistory/chat[1]/msg[1]/content[1]">
	<br/>
		<xsl:apply-templates select="content"/>
		<br/>
	</xsl:template>	
	
	<xsl:template match="to">
		<h1>
			<xsl:value-of select="text()"/>
		</h1>
		<br/>
	</xsl:template>
	
		<xsl:template match="from">
		<h1>
			<xsl:value-of select="text()"/>
		</h1>
		<br/>
	</xsl:template>
		<xsl:template match="time">
		<h1>
			<xsl:value-of select="text()"/>
		</h1>
		<br/>
	</xsl:template>


	
	<xsl:template match="content">
		<h1>
			<xsl:value-of select="text()"/>
		</h1>
		<br/>
	</xsl:template>
	
	
</xsl:stylesheet>
