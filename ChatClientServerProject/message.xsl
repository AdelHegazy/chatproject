<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Edited with XML Spy v2006 (http://www.altova.com) -->
<!-- in this example we use "apply-template" to over come the problem in 'cdcatalog_Without-apply.xsl' -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- Using xsl:apply template -->
	<xsl:template match="Messages">
		<html>
			<body>
				<h2>My CD Collection</h2>
				<table border="1">
					<tr bgcolor="#9acd32">
						<th>Color</th>
						<th>Font</th>
						<th>message</th>
						<!-- Must Exist to overide Default templates -->
					</tr>
					<xsl:apply-templates select="message"/>
				</table>
			</body>
		</html>
	</xsl:template>
	<!-- change calling order-->
	<xsl:template match="message">
		<tr>
			<xsl:call-template name="color"/>
			<xsl:call-template name="font"/>
			<xsl:call-template name="data"/>
			
		</tr>
	</xsl:template>	

	<xsl:template name="color">
		<td>
			<xsl:value-of select="color"/>
		</td>
	</xsl:template>
	<xsl:template name="font">
		<td>
			<xsl:value-of select="font"/>
		</td>
	</xsl:template>
	<xsl:template name="data">
		<td>
			<xsl:value-of select="data"/>
		</td>
	</xsl:template>
</xsl:stylesheet>


