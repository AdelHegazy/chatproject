/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.controller;

import static iti36.chat.controller.ChatClientServerProject.getInstance;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author adel
 */
public class AcceptFriendRequest {

    private static String message = "Friend Request was send to ";

    public static void accept(String accept, String sendAcceptTo) {
        try {
            getInstance().getObjServer().acceptRequest(accept, sendAcceptTo);
        } catch (RemoteException ex) {
            Logger.getLogger(Regestration.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
