/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.controller;

import static iti36.chat.controller.ChatClientServerProject.getInstance;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import iti36.chat.pojo.User;

/**
 *
 * @author adel
 */
public class ChangeStatus {

    public static void changeStatus(User user) {
        try {
            getInstance().getObjServer().changeUserStatus(user);
        } catch (RemoteException ex) {
            Logger.getLogger(Regestration.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
