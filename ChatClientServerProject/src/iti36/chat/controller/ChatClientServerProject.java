/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.controller;

import iti36.chat.util.ChangePanel;
import iti36.chat.interfaces.ClientOperationInt;
import iti36.chat.interfaces.ServerOperationIn;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;
import iti36.chat.model.ClientOperation;

/**
 *
 * @author adel
 */
public class ChatClientServerProject {

    private ClientOperationInt clientSession;

    private ServerOperationIn objServer;
    public static int PORT = 2002;
    public static String REGISTRY_NAME = "ChatService";
    public static ChatClientServerProject clientObj;
    public static Registry r;
    private String ip = "127.0.0.1";

    public static void main(String[] args) {
        getInstance().initConnection();
        ChangePanel.loginPanel();

    }

    public static synchronized ChatClientServerProject getInstance() {
        if (clientObj == null) {
            clientObj = new ChatClientServerProject();
        }
        return clientObj;
    }

    private void initConnection() {
        try {
            clientSession = new ClientOperation();
            r = LocateRegistry.getRegistry(ip, PORT);
            objServer = (ServerOperationIn) r.lookup(REGISTRY_NAME);
        } catch (RemoteException | NotBoundException ex) {
            Logger.getLogger(ChatClientServerProject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ServerOperationIn getObjServer() {
        return objServer;
    }

    public ClientOperationInt getClientSession() {
        return clientSession;
    }

    public void setClientSession(ClientOperationInt clientSession) {
        this.clientSession = clientSession;
    }

    public void registerIp() {

        initConnection();

    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

}
