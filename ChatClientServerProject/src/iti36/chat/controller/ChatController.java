/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.controller;

import iti36.chat.interfaces.ClientOperationInt;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import iti36.chat.pojo.ContactList;
import iti36.chat.pojo.User;
import iti36.chat.view.Massenger;

/**
 *
 * @author adel
 */
public class ChatController extends ChatClientServerProject {

    private static ChatController clientObj;
    ClientOperationInt friendContact;
    String serverLocalStore;
    File uFile;

    public void chatRegistration(ContactList contact) {
        try {
            friendContact = getInstance().getObjServer().sessionRegistration(contact);
        } catch (RemoteException ex) {
            Logger.getLogger(ChatController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendMessageToContact(String message, User user) {
        new Thread() {
            public void run() {
                try {
                    if (friendContact != null) {
                        friendContact.sendMessage(message, user.getEmail());
                    }
                } catch (RemoteException ex) {
                    Logger.getLogger(ChatController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }.start();
    }

    public void deliveredMessages(String message, String email) {
        new Thread() {
            public void run() {
                Massenger.getInstance().getNewMessage(email, message);
            }
        }.start();
    }

    public void sendFile(String path) {
        new Thread() {
            public void run() {
                File file = new File("C:\\upload");
                if (!file.exists()) {
                    if (file.mkdir()) {
                        System.out.println("Directory is created!");
                    } else {
                        System.out.println("Failed to create directory!");
                    }
                }
                File Test = new File(path);
                serverLocalStore = "C:\\upload\\" + Test.getName();
                System.out.println(serverLocalStore + "==========================");

                uFile = new File(serverLocalStore);
                System.out.println(uFile.getPath() + "==========================");
                try {
                    ChatClientServerProject.getInstance().getClientSession().upload(friendContact, Test, uFile);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        }.start();

    }

    public void download() {
        try {
            // File x = new File(serverLocalStore);
            ChatClientServerProject.getInstance().getClientSession().download();
        } catch (IOException ex) {
            Logger.getLogger(ChatController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean requestSendFile(String email) {
        if (friendContact != null) {
            try {
                return friendContact.sendFileAck(email);
            } catch (RemoteException ex) {
                Logger.getLogger(ChatController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

}
