/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.controller;

import iti36.chat.util.ChangePanel;
import static iti36.chat.controller.ChatClientServerProject.getInstance;
import iti36.chat.interfaces.UserConstant;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import iti36.chat.pojo.User;
import iti36.chat.view.Massenger;

/**
 *
 * @author adel
 */
public class LoginController extends ChatClientServerProject {

    private final String invalid = "invalid User name or password";
    private final String logged = "your accout is log in from another place";

    public void login(User user) {
        try {
            System.out.println(user.getEmail());
            System.out.println(user.getPassword());
            User loginUser = getInstance().getObjServer().login(user);
            if (loginUser != null) {
                if (loginUser.getStatus().equals(UserConstant.INVALID)) {
                    JOptionPane.showMessageDialog(ChangePanel.getFrame(), logged);
                } else {
                    getInstance().getObjServer().Register(getInstance().getClientSession(), loginUser);
                    user.setStatus(UserConstant.ONLINE);
                    Massenger login = Massenger.getInstance(loginUser);
                    login.setVisible(true);
                    ChangePanel.getFrame().dispose();
                    ChangeStatus.changeStatus(loginUser);
                    login.showFriendRequestMessage();
                }

            } else {
                JOptionPane.showMessageDialog(ChangePanel.getFrame(), invalid);
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Regestration.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
