package iti36.chat.controller;

import java.io.File;
import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author adel
 */
public class SaveChatController {

    public void saveChat(String message ,String name) {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            //for validarion with schema 
            docFactory.setValidating(false);
            docFactory.setNamespaceAware(true);

            SchemaFactory schemaFactoy = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

            docFactory.setSchema(schemaFactoy.newSchema(new Source[]{new StreamSource("ChatHistorySchema.xsd")}));
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            
            Document doc = docBuilder.newDocument();       
            Element chatHistory = doc.createElement("chatHistory");
            doc.appendChild(chatHistory);
            Element chat = doc.createElement("chat");
            Element to = doc.createElement("to");
            Element from = doc.createElement("from");
            Element time = doc.createElement("time");

            Element msg = doc.createElement("msg");

            chatHistory.appendChild(chat);
            chat.appendChild(to);
            chat.appendChild(from);
            chat.appendChild(time);
            chat.appendChild(msg);
            //for everyline you have to specify the sender

            Element sender = doc.createElement("sender");

            Element font = doc.createElement("font");
            Element size = doc.createElement("size");
            Element color = doc.createElement("color");
            Element bold = doc.createElement("bold");
            Element italic = doc.createElement("italic");
            Element content = doc.createElement("content");

            msg.appendChild(sender);
            msg.appendChild(font);
            msg.appendChild(size);
            msg.appendChild(color);
            msg.appendChild(bold);
            msg.appendChild(italic);
            msg.appendChild(content);

            to.setTextContent("to");
            from.setTextContent("from");
            time.setTextContent("time");
            sender.setTextContent("sender");

            font.setTextContent("Verdana");

            size.setTextContent("7");
            color.setTextContent("RED");
            bold.setTextContent("BOLD");
            italic.setTextContent("ITALIC");
            content.setTextContent(message);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
           Templates template = transformerFactory.newTemplates(new StreamSource(new FileInputStream("ChatHistoryXsd.xslt")));
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("ChatHistoryxml"+ name+".xml"));

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(source, result);

        } catch (Exception ex) {
            Logger.getLogger(SaveChatController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

    }
}
