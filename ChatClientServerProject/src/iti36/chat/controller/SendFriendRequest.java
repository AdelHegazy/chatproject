/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.controller;

import static iti36.chat.controller.ChatClientServerProject.getInstance;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import iti36.chat.view.Massenger;

/**
 *
 * @author adel
 */
public class SendFriendRequest {

    private static String message = "Friend Request was send to ";

    public static void send(String from, String to) {
        new Thread() {
            public void run() {
                try {
                    boolean resultRequest = getInstance().getObjServer().sendFriendRequest(from, to);
                    if (resultRequest) {
                        JOptionPane.showMessageDialog(Massenger.getInstance(), message + to);
                    }
                } catch (RemoteException ex) {
                    Logger.getLogger(Regestration.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }.start();

    }
}
