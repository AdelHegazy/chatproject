/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.controller;

import static iti36.chat.controller.ChatClientServerProject.getInstance;
import iti36.chat.interfaces.ClientOperationInt;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import iti36.chat.pojo.ContactList;
import iti36.chat.view.Massenger;

/**
 *
 * @author adel
 */
public class chatConference {

    private static ChatController clientObj;
    ClientOperationInt friendContact;
    List<ClientOperationInt> friendContactList;

    public void chatRegistration(List<ContactList> contact, String message) {
        new Thread() {
            public void run() {
                try {
                    getInstance().getObjServer().sessionRegistrationConference(contact, message, Massenger.getInstance().getCurrentUser().getEmail());
                } catch (RemoteException ex) {
                    Logger.getLogger(chatConference.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }.start();

    }
}
