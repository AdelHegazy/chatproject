/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.interfaces;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import iti36.chat.pojo.ContactList;
import iti36.chat.pojo.User;

/**
 *
 * @author adel
 */
public interface ClientOperationInt extends Remote {

    //Login user into server
    public User clientLogin(User user) throws RemoteException;

    //log out that make user not online  
    public boolean clientlogOut() throws RemoteException;

    //sign up new user
    public boolean clientRegestration() throws RemoteException;
//user can view online friends 

    public List<User> getOnLineUsers() throws RemoteException;
//user can change his statues online , away , ...

    public boolean changeUserStatus(User user) throws RemoteException;
//user feeling 

    public boolean changeUserMode(User user) throws RemoteException;
//user accept friend 

    public boolean acceptRequest(String user, String friendEmail) throws RemoteException;

    public boolean removeFriend(String user, String friendEmail) throws RemoteException;
//Add friend 

    public void sendFriendRequest(String user, String friendEmail) throws RemoteException;
//Block 

    public boolean blockFriend(String user, String friendEmail) throws RemoteException;
//unBlock

    public boolean unBlockFriend(String user, String friendEmail) throws RemoteException;

    public void sendMessage(String message, String emailSender) throws RemoteException;

    public void sendMessage(List<ContactList> contact, String message, String senderEmail) throws RemoteException;

    public void sendNotification(String email, String staus) throws RemoteException;

    public void upload(ClientOperationInt server, File src,
            File dest) throws IOException;

    public void download() throws IOException;

    public void copy(InputStream in, OutputStream out)
            throws IOException;

    public OutputStream getOutputStream(File f) throws IOException;

    public InputStream getInputStream(File f) throws IOException;

    public void reciveAnnouncementMessage(String message) throws RemoteException;

    public void updateUserInfo(User user) throws RemoteException;

    public boolean sendFileAck(String  email) throws RemoteException;

}
