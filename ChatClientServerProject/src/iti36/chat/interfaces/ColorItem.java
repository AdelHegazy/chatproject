/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.interfaces;

import java.awt.Color;

/**
 *
 * @author adel
 */
public class ColorItem {

    private Color color;
    private String colorName;

    public ColorItem(String colorName, Color color) {
        this.color = color;
        this.colorName = colorName;
    }

    public Color getColor() {
        return color;
    }

    public String getColorName() {
        return colorName;
    }
}
