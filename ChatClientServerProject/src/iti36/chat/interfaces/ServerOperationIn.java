/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.interfaces;

import iti36.chat.pojo.User;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import iti36.chat.pojo.ContactList;

/**
 *
 * @author adel
 */
public interface ServerOperationIn extends Remote {

    public User login(User user) throws RemoteException;

    public boolean logOut() throws RemoteException;

    public boolean regestration(User user) throws RemoteException;

    public List<User> getOnLineUsers() throws RemoteException;

    public boolean changeUserStatus(User user) throws RemoteException;

    public boolean changeUserMode(User user) throws RemoteException;

    public boolean acceptRequest(String user, String friendEmail) throws RemoteException;

    public boolean removeFriend(String user, String friendEmail) throws RemoteException;

    public boolean sendFriendRequest(String user, String friendEmail) throws RemoteException;

    public boolean blockFriend(String user, String friendEmail) throws RemoteException;

    public boolean unBlockFriend(String user, String friendEmail) throws RemoteException;

    public List<User> getAllUsers() throws RemoteException;

    public void Register(ClientOperationInt client, User user) throws RemoteException;

    public ClientOperationInt sessionRegistration(ContactList client) throws RemoteException;

    public void sessionRegistrationConference(List<ContactList> contact,String message,String senderEmail) throws RemoteException;

}
