/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.interfaces;

/**
 *
 * @author adel
 */
public interface UserConstant {

    public final static String MALE = "male";
    public final static String FEMALE = "female";
    public final static String ONLINE = "online";
    public final static String INVIBLE = "invisble";
    public final static String OFFLINE = "offline";
    public final static String BUSY = "busy";
    public final static String AWAY = "away";
    public final static String INVALID = "invalid";

}
