/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.model;

import iti36.chat.controller.AcceptFriendRequest;
import iti36.chat.controller.ChatController;
import iti36.chat.interfaces.ClientOperationInt;
import iti36.chat.rmi.io.RMIInputStream;
import iti36.chat.rmi.io.RMIInputStreamImpl;
import iti36.chat.rmi.io.RMIOutputStream;
import iti36.chat.rmi.io.RMIOutputStreamImpl;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import iti36.chat.pojo.ContactList;
import iti36.chat.pojo.User;
import iti36.chat.view.ConfernceChat;
import iti36.chat.view.Massenger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author adel
 */
public class ClientOperation extends UnicastRemoteObject implements ClientOperationInt {

    public ClientOperation() throws RemoteException {

    }

    @Override
    public User clientLogin(User user) throws RemoteException {
//        Massenger login = new Massenger();
//        login.setVisible(true);
        return user;
    }

    @Override
    public boolean clientlogOut() throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean clientRegestration() throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<User> getOnLineUsers() throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean changeUserStatus(User user) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean changeUserMode(User user) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean acceptRequest(String user, String friendEmail) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean removeFriend(String user, String friendEmail) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void sendFriendRequest(String from, String to) throws RemoteException {
        int result = JOptionPane.showConfirmDialog(Massenger.getInstance(),
                from + " send friend request to u ! ", "FriendRequest",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
        if (result == 0) {
            AcceptFriendRequest.accept(to, from);
        } //if user online
    }

    @Override
    public boolean blockFriend(String user, String friendEmail) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean unBlockFriend(String user, String friendEmail) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void sendMessage(String message, String emailSender) throws RemoteException {
        new ChatController().deliveredMessages(message, emailSender);
    }

    @Override
    public void sendMessage(List<ContactList> contact, String message, String emailSender) throws RemoteException {
        new Thread() {
            public void run() {
                ConfernceChat.getInstance().getAddFriendToConfraence().setVisible(false);
                if (ConfernceChat.getInstance().getjTextAreaChatConfernace().getText().toString().isEmpty()) {
                    ConfernceChat.getInstance().getjTextAreaChatConfernace().setText(ConfernceChat.getInstance().getjTextAreaChatConfernace().getText().toString() + message);
                } else {
                    ConfernceChat.getInstance().getjTextAreaChatConfernace().setText(ConfernceChat.getInstance().getjTextAreaChatConfernace().getText().toString() + "\n" + message);
                }

                DefaultListModel modelFriend = new DefaultListModel();
                for (int i = 0; i < contact.size(); i++) {
                    modelFriend.addElement(contact.get(i).getEmail());
                }
                modelFriend.addElement(emailSender);
                ConfernceChat.getInstance().getChatUsers().setModel(modelFriend);
            }
        }.start();

    }

    @Override
    public void sendNotification(String email, String staus) throws RemoteException {
        for (int i = 0; i < Massenger.getInstance().getCurrentUser().getContact().size(); i++) {
            if (Massenger.getInstance().getCurrentUser().getContact().get(i).getEmail().equals(email)) {
                Massenger.getInstance().getCurrentUser().getContact().get(i).setStatus(staus);
                Massenger.getInstance().fillData(Massenger.getInstance().getCurrentUser().getContact());
            }
        }

    }

    //fileTrnasfer
    //final public static int BUF_SIZE = 1024 * 64;
    public static int BUF_SIZE = 2048 * 64;

    public void copy(InputStream in, OutputStream out)
            throws IOException {
        System.out.println("Copying Data ...");
        byte[] b = new byte[BUF_SIZE];
        int length;
        while ((length = in.read(b)) >= 0) {
            out.write(b, 0, length);
        }
        in.close();
        out.close();
    }
    File src;
    File dest;
    ClientOperationInt client;

    @Override
    public void upload(ClientOperationInt client, File src, File dest) throws IOException {
        this.src = src;
        this.dest = dest;
        this.client = client;
        new Thread() {
            public void run() {
                try {
                    File file = new File("C:\\upload");
                    if (!file.exists()) {
                        if (file.mkdir()) {
                            System.out.println("Directory is created!");
                        } else {
                            System.out.println("Failed to create directory!");
                        }
                    }
                    copy(new FileInputStream(src),
                            client.getOutputStream(dest));

                } catch (Exception ex) {
                    Logger.getLogger(ChatController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }.start();
    }

    @Override
    public void download() throws IOException {
        copy(client.getInputStream(dest),
                new FileOutputStream(src));
    }

    public OutputStream getOutputStream(File f) throws IOException {

        return new RMIOutputStream(new RMIOutputStreamImpl(new FileOutputStream(f)));
    }

    public InputStream getInputStream(File f) throws IOException {
        return new RMIInputStream(new RMIInputStreamImpl(new FileInputStream(f)));
    }

    @Override
    public void reciveAnnouncementMessage(String message) {
        Massenger.getInstance().reciveAnnouncementMessage(message);
    }

    @Override
    public void updateUserInfo(User user) throws RemoteException {
        Massenger.getInstance().setCurrentUser(user);
        Massenger.getInstance().fillData(user.getContact());
    }

    @Override
    public boolean sendFileAck(String email) throws RemoteException {
        if (Massenger.getInstance().reciveFileAck(email) == 1) {
            return false;
        } else {
            return true;
        }
    }
}
