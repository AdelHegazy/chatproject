/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author adel
 */
//@manar w need to but lists or vectors of objects of friends/friendRequests/block/...
public class User implements Serializable {

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String mode;
    private String gender;
    private String status;
    private Date lastSeen;
    private boolean showLastSeen;
    private String Country;
    private String city;
    private Date criationDate;
    private String url;
    private List<ContactList> contact = new ArrayList<>();
    private List<BlockRequest> blockedUser = new ArrayList<>();
    private List<FriendRequest> friendRequest = new ArrayList<>();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isShowLastSeen() {
        return showLastSeen;
    }

    public void setShowLastSeen(boolean showLastSeen) {
        this.showLastSeen = showLastSeen;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public Date getCriationDate() {
        return criationDate;
    }

    public void setCriationDate(Date criationDate) {
        this.criationDate = criationDate;
    }

    public List<ContactList> getContact() {
        return contact;
    }

    public void addContact(ContactList contact) {
        this.contact.add(contact);
    }

    public List<BlockRequest> getBlockedUser() {
        return blockedUser;
    }

    public void addBlockedUser(BlockRequest blockedUser) {
        this.blockedUser.add(blockedUser);
    }

    public List<FriendRequest> getFriendRequest() {
        return friendRequest;
    }

    public void addFriendRequest(FriendRequest friendRequest) {
        this.friendRequest.add(friendRequest);
    }
}
