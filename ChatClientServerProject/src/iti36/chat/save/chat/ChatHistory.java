/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.save.chat;

import iti36.chat.view.ClientChat;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author adel
 */
public class ChatHistory {

    private JAXBContext context;
    private JAXBElement JAXBMessage;
    private MessagesType messageType;
    private JAXBElement messageElement;
    private List<MessageTagType> messageElementList;
    private static ChatHistory instance;

    public static ChatHistory getInstance() {
        if (instance == null) {
            instance = new ChatHistory();
        }
        return instance;
    }

    public void readHistory(String fileName, ClientChat ob) throws IOException {

        try {
            File f = new File(fileName + ".xml");
            System.out.println("iti36.chat.save.chat.ChatHistory.readHistory()" + f.getPath());
            if (f.exists()) {
                read(f, ob);
            }
        } catch (JAXBException ex) {
            Logger.getLogger(ChatHistory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void read(File f, ClientChat ob) throws JAXBException {
        System.out.println("iti36.chat.save.chat.ChatHistory.read()sss");
        context = JAXBContext.newInstance("iti36.chat.save.chat");
        Unmarshaller unmarsh = context.createUnmarshaller();
        JAXBMessage = (JAXBElement) unmarsh.unmarshal(f);
        messageType = (MessagesType) JAXBMessage.getValue();
        messageElementList = messageType.getMessage();
        for (int i = 0; i < messageElementList.size(); i++) {
            if (!ob.getDeliveredMessages().getText().isEmpty()) {
                ob.getDeliveredMessages().setText(ob.getDeliveredMessages().getText() + "\n" + messageElementList.get(i).getData());
            } else {
                ob.getDeliveredMessages().setText(messageElementList.get(i).getData());

            }
        }

    }

    public static void main(String[] arg) {
        new ChatHistory();

    }

    public void saveHistory(String color, String font, String message, String fileName) {
        try {
            List addressList = messageType.getMessage();
            ObjectFactory factory = new ObjectFactory();
            MessageTagType newMessage = factory.createMessageTagType();
            newMessage.setColor(color);
            newMessage.setFont(font);
            newMessage.setData(message);
            addressList.add(newMessage);
            messageElement = factory.createMessages(messageType);
            Marshaller marsh = context.createMarshaller();
            marsh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marsh.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, "messageSchema.xsd");
            marsh.setProperty("com.sun.xml.internal.bind.xmlHeaders",
                    "<?xml-stylesheet type='text/xsl' href='message.xsl' ?>");
            marsh.marshal(messageElement, new FileOutputStream(fileName + ".xml"));
        } catch (JAXBException ex) {
            Logger.getLogger(ChatHistory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ChatHistory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
