/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.util;

import javax.swing.JFrame;
import javax.swing.JPanel;
import iti36.chat.view.LoginFrame;
import javax.swing.UIManager;
import de.javasoft.plaf.synthetica.SyntheticaBlackStarLookAndFeel;

/**
 *
 * @author adel
 */
public class ChangePanel {

    private static JFrame frame;
    private static JPanel panel;

    public static void changePanel(JPanel panel) {
        if (ChangePanel.panel != null) {
            frame.remove(ChangePanel.panel);
        }
        ChangePanel.panel = panel;
        frame.add(ChangePanel.panel);
        frame.revalidate();
    }

    public static void loginPanel() {
         lookAndFell();
        frame = new JFrame();
        LoginFrame login = new LoginFrame();
        login.setOpaque(true);
        changePanel(login);
        frame.setSize(500, 550);
        frame.setResizable(false);
        frame.setVisible(true);
    }

    private static void lookAndFell() {
        try {
            UIManager.setLookAndFeel(new SyntheticaBlackStarLookAndFeel());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JFrame getFrame() {
        return frame;
    }

    public static void setFrame(JFrame frame) {
        ChangePanel.frame = frame;
    }

    public static JPanel getPanel() {
        return panel;
    }

    public static void setPanel(JPanel panel) {
        ChangePanel.panel = panel;
    }

}
