package iti36.chat.view;

import iti36.chat.controller.ChatClientServerProject;
import iti36.chat.controller.Regestration;
import java.util.Date;
import iti36.chat.pojo.User;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFrame;

public class GUI extends javax.swing.JPanel 
{

    boolean signOk = true;
    private Pattern pattern;
    private Matcher matcher;

    private static final String EMAIL_PATTERN
            = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public GUI() {

        initComponents();
        /*
        String [] date = new String[31];
        for(int i = 0; i < 31; i++) 
        {
        date[i] =(String) i + 1;
        }
        Day.setModel(new javax.swing.DefaultComboBoxModel(date));
        */
        
        System.out.println("hi");

        /* 
        try 
        {
          
            
           Registry reg = LocateRegistry.getRegistry("172.0.0.1", 2002);
            ServerOperationIn SORef = (ServerOperationIn)reg.lookup("ChatService");
        } 
        catch (RemoteException | NotBoundException ex) 
        {
            Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } 
        
         */
    }

    public boolean validate(final String hex) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(hex);
        return matcher.matches();
    }

    /**
     * 1 name not empty 2 password not less than 8 3 password and confirmation
     * match 4 mail valid // there is regex for it 5 phone id all numbers or - /
     * check length 6 all data entered // mostly not interested in optional 7 8
     * 9 10
     */
    public void validateAll() 
    {
        User newUser = new User();
        //signOk = true;
        passMsg.setText("");
        confMsg.setText("");
        
        
        
        if (firstName.getText().isEmpty()) 
        {
            firstName.setText("You Must Enter Your Last Name");
            signOk = false;
        }
        if (lastName.getText().isEmpty()) 
        {
            lastName.setText("You Must Enter Your First Name");
            signOk = false;
        }
        if (email.getText().isEmpty()) {
           
            email.setText("*You Must Enter Your Mail");
            
            signOk = false;
        } 
        
        if (!email.getText().isEmpty()&&!validate(email.getText())) 
        {
            email.setText("*This EMail Is Not Valid");
            signOk = false;
        }
        //Validate if is the email being used or is it empty 
         if (Password.getText().isEmpty()) 
         {
            passMsg.setText("You Must Enter Your Password");
            signOk = false;
        } 
         if (ConfirmPassword.getText().isEmpty()) 
         {
            confMsg.setText("You Must Enter Your Confiramtion of the Password");
            signOk = false;
        }
        if (!Password.getText().isEmpty()&&((Password.getText().length() < 8)||(ConfirmPassword.getText().length() < 8)))
        {
            passMsg.setText("Password has to be 8 digits at least ");
            signOk = false;
        }
        if (!Password.getText().isEmpty()&&!ConfirmPassword.getText().isEmpty()&&!Password.getText().equals(ConfirmPassword.getText())) 
        {
            passMsg.setText("Password Doesn't Match :( You Must Enter It Again ");
            signOk = false;
        } 
        if(!phone.getText().isEmpty())
        {
            
        }
       // if() check for gender check cant choose both
       //if city
       //if country
        
            
        
       if(!FaceBook.getText().isEmpty())
       {
          //check does it even exits or not 
       }
       if(!Twitter.getText().isEmpty())
       {
           //check does it even exits or not
       }
      
       
        if(signOk==true)
        {
            //Progress.setText(" Sign Up ..... ");
            
            newUser.setEmail(email.getText().trim());
            newUser.setLastName(lastName.getText().trim());
            newUser.setFirstName(firstName.getText().trim());
            newUser.setLastSeen(new Date());
            newUser.setPassword(Password.getText());
            new Regestration().signUP(newUser);
        }
       
    }

    public void dataObjectSignUp() {
        User dataObject = new User();

        if (signOk == true) {
            dataObject.setFirstName(firstName.getText());
            dataObject.setLastName(lastName.getText());
            dataObject.setEmail(email.getText());
            dataObject.setPassword(Password.getText());
            dataObject.setGender(TOOL_TIP_TEXT_KEY);
            dataObject.setCountry(TOOL_TIP_TEXT_KEY);
            dataObject.setCity(TOOL_TIP_TEXT_KEY);
            dataObject.setUrl(TOOL_TIP_TEXT_KEY);
            //dataObject.setDataCreat();
            //dataObject.setPhone();
            //dataObject.setBirthDay();
            //dataObject.set
            //dataObject.set
            //dataObject.set
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        label1 = new java.awt.Label();
        label2 = new java.awt.Label();
        lastName = new javax.swing.JTextField();
        firstName = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        email = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        Day = new javax.swing.JComboBox<>();
        Month = new javax.swing.JComboBox<>();
        Year = new javax.swing.JComboBox<>();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabel7 = new javax.swing.JLabel();
        phone = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        Country = new javax.swing.JComboBox<>();
        City = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        FaceBook = new javax.swing.JTextField();
        Twitter = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        Password = new javax.swing.JPasswordField();
        ConfirmPassword = new javax.swing.JPasswordField();
        passMsg = new javax.swing.JLabel();
        confMsg = new javax.swing.JLabel();

        setBackground(new java.awt.Color(8, 60, 83));
        setForeground(new java.awt.Color(255, 255, 255));
        setToolTipText("E-Mail");
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setText("Ok/Submit/Sign Up");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 480, 137, 41));

        label1.setForeground(new java.awt.Color(255, 255, 255));
        label1.setText("First Name");
        add(label1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 63, 70, -1));

        label2.setForeground(new java.awt.Color(255, 255, 255));
        label2.setText("Last Name");
        add(label2, new org.netbeans.lib.awtextra.AbsoluteConstraints(344, 63, 70, -1));

        lastName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lastNameActionPerformed(evt);
            }
        });
        add(lastName, new org.netbeans.lib.awtextra.AbsoluteConstraints(434, 63, 180, -1));

        firstName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                firstNameActionPerformed(evt);
            }
        });
        add(firstName, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 63, 180, -1));

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("E-Mail");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 117, 70, 20));
        add(email, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 117, 380, -1));

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Password");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 180, 70, 20));

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Confirm Password");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(344, 180, -1, 20));

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Gender");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(47, 280, -1, 20));

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("BirthDay");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(355, 280, -1, 20));

        Day.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4","5","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","26","27","28","29","30","31" }));
        Day.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DayActionPerformed(evt);
            }
        });
        add(Day, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 280, 50, 20));

        Month.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1 January", "2 February", "3 March", "4 April","5 May","6 June","7 July","8 August","9 September","10 October","11 November","12 December" }));
        add(Month, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 280, 90, -1));

        Year.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1990", "1991", "1992", "1992","1993","1994", "1995", "1996", "1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016"  }));
        add(Year, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 280, 70, 20));

        jRadioButton1.setText("Female");
        add(jRadioButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 279, 71, -1));

        jRadioButton2.setText("Male");
        add(jRadioButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(189, 279, 57, -1));

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Phone");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(47, 335, 43, -1));

        phone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                phoneActionPerformed(evt);
            }
        });
        add(phone, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 330, 110, -1));

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Country");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 380, 60, 20));

        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Ciity");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 370, 70, 20));

        Country.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Egypt", "England", "USA", "Spain","South Korea","India","France","UAE","","" }));
        Country.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CountryActionPerformed(evt);
            }
        });
        add(Country, new org.netbeans.lib.awtextra.AbsoluteConstraints(136, 380, 80, -1));

        City.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Cairo", "London", "New Delhi", "Madrid","New York","Seol","Dubai","","" }));
        add(City, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 380, 80, -1));

        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("FaceBook");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 430, 70, 20));

        FaceBook.setText("please enter your facebook account");
        FaceBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FaceBookActionPerformed(evt);
            }
        });
        add(FaceBook, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 430, 200, 20));

        Twitter.setText("please enter your twitter acount");
        Twitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TwitterActionPerformed(evt);
            }
        });
        add(Twitter, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 430, 210, 20));

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Twitter");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 430, 50, 20));

        Password.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PasswordActionPerformed(evt);
            }
        });
        add(Password, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 180, 100, -1));

        ConfirmPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConfirmPasswordActionPerformed(evt);
            }
        });
        add(ConfirmPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 180, 120, -1));

        passMsg.setFont(new java.awt.Font("Vrinda", 3, 14)); // NOI18N
        add(passMsg, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 220, 220, 30));
        add(confMsg, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 220, 230, 30));
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        validateAll();

    }//GEN-LAST:event_jButton1ActionPerformed

    private void firstNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_firstNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_firstNameActionPerformed

    private void lastNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lastNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lastNameActionPerformed

    private void DayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DayActionPerformed
        // TODO add your handling code here:
        //Item 1 = new Item();


    }//GEN-LAST:event_DayActionPerformed

    private void phoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_phoneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_phoneActionPerformed

    private void FaceBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FaceBookActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_FaceBookActionPerformed

    private void TwitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TwitterActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TwitterActionPerformed

    private void CountryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CountryActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CountryActionPerformed

    private void PasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PasswordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PasswordActionPerformed

    private void ConfirmPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConfirmPasswordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ConfirmPasswordActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> City;
    private javax.swing.JPasswordField ConfirmPassword;
    private javax.swing.JComboBox<String> Country;
    private javax.swing.JComboBox<String> Day;
    private javax.swing.JTextField FaceBook;
    private javax.swing.JComboBox<String> Month;
    private javax.swing.JPasswordField Password;
    private javax.swing.JTextField Twitter;
    private javax.swing.JComboBox<String> Year;
    private javax.swing.JLabel confMsg;
    private javax.swing.JTextField email;
    private javax.swing.JTextField firstName;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private java.awt.Label label1;
    private java.awt.Label label2;
    private javax.swing.JTextField lastName;
    private javax.swing.JLabel passMsg;
    private javax.swing.JTextField phone;
    // End of variables declaration//GEN-END:variables

    public static void main(String[] args) {
        JFrame ob = new JFrame();

        GUI gui = new GUI();
        ob.setSize(700, 600);
        //ob.pack();
        gui.setVisible(true);
        ob.add(gui);
        //JCalender bd = new JCalender();

        ob.setVisible(true);
        boolean b;
        b = gui.isVisible();

        System.out.println(b);

        //this.setIconImage(Toolkit.getDefaultToolkit().getImage(""));
    }

}
