/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.view;

import de.javasoft.plaf.synthetica.SyntheticaBlackStarLookAndFeel;
import iti36.chat.controller.AcceptFriendRequest;
import iti36.chat.controller.ChangeStatus;
import iti36.chat.controller.ChatController;
import iti36.chat.interfaces.UserConstant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import iti36.chat.pojo.ContactList;
import iti36.chat.pojo.User;
import iti36.chat.save.chat.ChatHistory;
import iti36.chat.util.ChangePanel;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.UIManager;

/**
 *
 * @author adel
 */
public class Massenger extends javax.swing.JFrame {

    /**
     * Creates new form NewJFrame
     */
//    private int positon = 0;
    private User CurrentUser;

    private Map<String, ClientChat> currentOpenScreen = new HashMap<String, ClientChat>();
    private static Massenger instance;
    public static Map<String, ChatController> currentSessionChat = new HashMap<String, ChatController>();

    public static void setInstance(Massenger instance) {
        Massenger.instance = instance;
    }

    private Massenger(User user) {
        initComponents();
        setResizable(false);
        CurrentUser = user;
        clientName.setText(user.getFirstName() + " " + user.getLastName());
        userStatus.setText(user.getStatus());
        fillData(CurrentUser.getContact());
        close();

    }
    private void close (){
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
       addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
              setExtendedState(JFrame.ICONIFIED);
            }
        });
    }

    public void showFriendRequestMessage() {
        if (CurrentUser != null && CurrentUser.getFriendRequest() != null) {
            for (int i = 0; i < CurrentUser.getFriendRequest().size(); i++) {
                int result = JOptionPane.showConfirmDialog(Massenger.getInstance(),
                        CurrentUser.getFriendRequest().get(i).getEmailSender() + " send friend request to u ! ", "FriendRequest",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                if (result == 0) {
                    AcceptFriendRequest.accept(CurrentUser.getEmail(), CurrentUser.getFriendRequest().get(i).getEmailSender());
                }
            }
        }

    }

    public static synchronized Massenger getInstance(User user) {
        if (instance == null) {
            instance = new Massenger(user) {
            };
        }
        return instance;
    }

    public static Massenger getInstance() {
        return instance;
    }

    public Map<String, ClientChat> getMyMap() {
        return currentOpenScreen;
    }

    public void fillData(List<ContactList> contac) {
        DefaultMutableTreeNode contact = new DefaultMutableTreeNode("ContactList");
        DefaultMutableTreeNode online = new DefaultMutableTreeNode(UserConstant.ONLINE);
        DefaultMutableTreeNode offline = new DefaultMutableTreeNode(UserConstant.OFFLINE);
        DefaultMutableTreeNode away = new DefaultMutableTreeNode(UserConstant.AWAY);
        DefaultMutableTreeNode busy = new DefaultMutableTreeNode(UserConstant.BUSY);
        for (int i = 0; i < contac.size(); i++) {
            if (contac.get(i).getStatus().equals(UserConstant.ONLINE)) {
                online.add(new DefaultMutableTreeNode(contac.get(i).getEmail()));
            } else if (contac.get(i).getStatus().equals(UserConstant.AWAY)) {
                away.add(new DefaultMutableTreeNode(contac.get(i).getEmail()));
            } else if (contac.get(i).getStatus().equals(UserConstant.BUSY)) {
                busy.add(new DefaultMutableTreeNode(contac.get(i).getEmail()));
            } else if (contac.get(i).getStatus().equals(UserConstant.OFFLINE) || contac.get(i).getStatus().equals(UserConstant.INVIBLE)) {
                offline.add(new DefaultMutableTreeNode(contac.get(i).getEmail()));
            }
        }
        contact.add(online);
        contact.add(busy);
        contact.add(away);
        contact.add(offline);

        DefaultTreeModel root = new DefaultTreeModel(contact);
        contactJtree.setModel(root);
        contactJtree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {

            @Override
            public void valueChanged(TreeSelectionEvent e) {
                if (contactJtree.isSelectionEmpty()) {
                    System.out.println(".valueChanged()");
                    return;
                }
                TreePath tp = contactJtree.getSelectionPath();
                DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tp.getLastPathComponent();
                String selectedContact = selectedNode.getUserObject().toString();
                if (!selectedContact.equals(UserConstant.AWAY) && !selectedContact.equals(UserConstant.BUSY)
                        && !selectedContact.equals(UserConstant.ONLINE) && !selectedContact.equals(UserConstant.OFFLINE) && !selectedContact.equals("ContactList")) {
                    if (currentOpenScreen.get(selectedContact) == null) {
                        System.out.println(".valueChanged()" + selectedContact);
                        ContactList contact = getUser(selectedContact);
                        ClientChat obj = new ClientChat(contact);
                        obj.setVisible(true);
                        currentOpenScreen.put(selectedContact, obj);
                        // regestration Session peer to peer work in online mode
                        // ChatController session = new ChatController(selectedContact);
                        ChatController session = new ChatController();
                        session.chatRegistration(contact);
                        currentSessionChat.put(contact.getEmail(), session);

                    } else {
                        System.out.println(currentOpenScreen.get(selectedContact));
                    }
                    contactJtree.clearSelection();
                }
            }
        });
    }

    public void reciveAnnouncementMessage(String message) {
        JOptionPane.showMessageDialog(Massenger.getInstance(), message);
    }

    public void getNewMessage(String senderEmail, String message) {
        ClientChat ob;
        if (currentOpenScreen.get(senderEmail) == null) {
            ContactList contact = getUser(senderEmail);
            ClientChat chat = new ClientChat(contact);
            chat.setVisible(true);
            currentOpenScreen.put(senderEmail, chat);
            //peer to peer
            ChatController session = new ChatController();
            session.chatRegistration(contact);
            currentSessionChat.put(contact.getEmail(), session);
            ob = chat;

            readIncomingMessage(senderEmail + "--" + Massenger.getInstance().getCurrentUser().getEmail(), ob);
            chat.getDeliveredMessages().setText(chat.getDeliveredMessages().getText() + " \n" + message);
            if (ob.getjCheckBox1().isSelected()) {
                saveIncomingMessage(message, ob, senderEmail);
            }
        } else {
            ob = currentOpenScreen.get(senderEmail);
            currentOpenScreen.get(senderEmail).getDeliveredMessages()
                    .setText(currentOpenScreen.get(senderEmail).getDeliveredMessages().getText() + " \n" + message);
            if (ob.getjCheckBox1().isSelected()) {
                saveIncomingMessage(message, ob, senderEmail);
            }
        }
        if (message.contains("##..##..##file")) {
            ob.getDeliveredMessages().setText(ob.getDeliveredMessages().getText() + " \n" + "File is sending");
        }
    }

    private void saveIncomingMessage(String mesage, ClientChat ob, String email) {
        if (ob.getjCheckBox1().isSelected()) {
            ChatHistory.getInstance().saveHistory("", "", mesage, email
                    + "--" + Massenger.getInstance().getCurrentUser().getEmail());
        }
    }

    private void readIncomingMessage(String path, ClientChat ob) {
        try {
            ChatHistory.getInstance().readHistory(path, ob);
        } catch (IOException ex) {
            Logger.getLogger(Massenger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private ContactList getUser(String email) {
        ContactList contact = new ContactList();
        for (int i = 0; i < CurrentUser.getContact().size(); i++) {
            if (CurrentUser.getContact().get(i).getEmail().equals(email)) {
                contact = CurrentUser.getContact().get(i);
            }
        }
        return contact;
    }

    public User getCurrentUser() {
        return CurrentUser;
    }

    public void setCurrentUser(User CurrentUser) {
        this.CurrentUser = CurrentUser;
    }

    public int reciveFileAck(String email) {
        return JOptionPane.showConfirmDialog(Massenger.getInstance(), email + " send to u file ");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu4 = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();
        jPanel1 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        userStatus = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        clientName = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jTextField1 = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        contactJtree = new javax.swing.JTree();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        Online = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        ChatConferenceMenuItem = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        addContact = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();

        jMenu4.setText("File");
        jMenuBar2.add(jMenu4);

        jMenu5.setText("Edit");
        jMenuBar2.add(jMenu5);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 102, 51));
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.X_AXIS));

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 255, 0)));
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.Y_AXIS));

        userStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        userStatus.setText("online");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iti36/chat/pic/Sunset.jpg"))); // NOI18N

        clientName.setForeground(new java.awt.Color(255, 153, 102));
        clientName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        clientName.setText("Ade Hegazy");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(clientName, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(userStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)))
                .addContainerGap(54, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(clientName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(userStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(24, 24, 24))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12))))
        );

        jPanel1.add(jPanel5);

        jTextField1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField1.setForeground(new java.awt.Color(102, 102, 102));
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField1.setText("Search ...");
        jTextField1.setToolTipText("");
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel2);

        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.Y_AXIS));

        jScrollPane4.setViewportView(contactJtree);

        jPanel3.add(jScrollPane4);

        jPanel1.add(jPanel3);

        getContentPane().add(jPanel1);

        jMenuBar1.setBackground(new java.awt.Color(5, 85, 88));

        jMenu1.setForeground(new java.awt.Color(3, 54, 54));
        jMenu1.setText("Messanger");

        jMenu3.setText("Status");

        Online.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        Online.setText("Online");
        Online.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OnlineActionPerformed(evt);
            }
        });
        jMenu3.add(Online);

        jMenuItem6.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem6.setText("Busy");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem6);

        jMenuItem7.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem7.setText("Away");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem7);

        jMenuItem8.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem8.setText("Invisble");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem8);

        jMenu1.add(jMenu3);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("My Profile");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        ChatConferenceMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        ChatConferenceMenuItem.setText("Chat Conference");
        ChatConferenceMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChatConferenceMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(ChatConferenceMenuItem);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Sign Out");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setForeground(new java.awt.Color(3, 54, 54));
        jMenu2.setText("Contacts");

        addContact.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        addContact.setText("Add Contact");
        addContact.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addContactActionPerformed(evt);
            }
        });
        jMenu2.add(addContact);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setText("Block Contact");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addContactActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addContactActionPerformed
        new AddFriend(getCurrentUser()).setVisible(true);
    }//GEN-LAST:event_addContactActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void OnlineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OnlineActionPerformed
        Massenger.getInstance().getCurrentUser().setStatus(UserConstant.ONLINE);
        ChangeStatus.changeStatus(Massenger.getInstance().getCurrentUser());
        userStatus.setText(UserConstant.ONLINE);

    }//GEN-LAST:event_OnlineActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        // TODO add your handling code here:
        Massenger.getInstance().getCurrentUser().setStatus(UserConstant.BUSY);
        ChangeStatus.changeStatus(Massenger.getInstance().getCurrentUser());
        userStatus.setText(UserConstant.BUSY);
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        Massenger.getInstance().getCurrentUser().setStatus(UserConstant.OFFLINE);
        ChangeStatus.changeStatus(Massenger.getInstance().getCurrentUser());
        dispose();
        ChangePanel.loginPanel();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void ChatConferenceMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChatConferenceMenuItemActionPerformed
        ConfernceChat.getInstance().setContact(CurrentUser.getContact());
        ConfernceChat.getInstance().setVisible(true);

    }//GEN-LAST:event_ChatConferenceMenuItemActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        Massenger.getInstance().getCurrentUser().setStatus(UserConstant.AWAY);
        ChangeStatus.changeStatus(Massenger.getInstance().getCurrentUser());
        userStatus.setText(UserConstant.AWAY);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        Massenger.getInstance().getCurrentUser().setStatus(UserConstant.INVIBLE);
        ChangeStatus.changeStatus(Massenger.getInstance().getCurrentUser());
        userStatus.setText(UserConstant.INVIBLE);
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Massenger.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Massenger.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Massenger.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Massenger.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new Massenger().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem ChatConferenceMenuItem;
    private javax.swing.JMenuItem Online;
    private javax.swing.JMenuItem addContact;
    private javax.swing.JLabel clientName;
    private javax.swing.JTree contactJtree;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel userStatus;
    // End of variables declaration//GEN-END:variables
}
