/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.charts;

/**
 *
 * @author Hosam
 */
import iti36.chat.dao.DatabaseOperationIn;
import iti36.chat.dao.impl.DatabaseOperation;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;



public class StatusChart extends JFrame {
    int on,off,busy,away;
  private static final long serialVersionUID = 1L;
  DatabaseOperationIn databaseOperation;
  
  public StatusChart(String applicationTitle, String chartTitle) {
        super(applicationTitle);
        // This will create the dataset 
        databaseOperation = DatabaseOperation.getInstance();
        on=databaseOperation.getOnline();
        off=databaseOperation.getOffline();
        away=databaseOperation.getAway();
        busy=databaseOperation.getBusy();
        PieDataset dataset = createDataset();
        // based on the dataset we create the chart
        JFreeChart chart = createChart(dataset, chartTitle);
        // we put the chart into a panel
        ChartPanel chartPanel = new ChartPanel(chart);
        // default size
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        // add it to our application
        setContentPane(chartPanel);

    }
    
    
/**
     * Creates a sample dataset 
     */

    private  PieDataset createDataset() {
        DefaultPieDataset result = new DefaultPieDataset();
        result.setValue("Online users",on);
        result.setValue("offline users", off);
        result.setValue("busy users", busy);
        result.setValue("away users", away);
          return result;
        
    }
    
    
/**
     * Creates a chart
     */

    private JFreeChart createChart(PieDataset dataset, String title) {
        
        JFreeChart chart = ChartFactory.createPieChart3D(title,          // chart title
            dataset,                // data
            true,                   // include legend
            true,
            false);

        PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
        return chart;
        
    }
}
