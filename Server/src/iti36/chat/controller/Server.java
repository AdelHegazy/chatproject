/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.controller;
import iti36.chat.interfaces.Constatnt;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

/**
 *
 * @author adel
 */
public class Server {

    private Registry registry;
    private ServerOperation obj;
    private static Server instance;
    private JMXServiceURL url;
    JMXConnectorServer connector;

    private Server() {
      //  startServer();
    }

    public static synchronized Server getInstance() {
        if (instance == null) {
            instance = new Server() {
            };
        }
        return instance;
    }

    public void startServer() {
        try {
            obj = new ServerOperation();
            registry = LocateRegistry.createRegistry(Constatnt.PORT);
            registry.rebind(Constatnt.REGISTRY_NAME, obj);
            System.out.println("Server is running.");
        } catch (RemoteException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            System.out.print("here the err");

            try {
                this.connector = JMXConnectorServerFactory.newJMXConnectorServer(url,
                        new HashMap<String, Object>(),
                        ManagementFactory.getPlatformMBeanServer());
            } catch (IOException e) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {

            } catch (Exception e) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void stopServer() {
        try {

            registry.unbind(Constatnt.REGISTRY_NAME);
            if (connector != null) {
                connector.stop();
            }
// deregister the registry
            if (registry != null) {
                UnicastRemoteObject.unexportObject(registry, true);
            }

            System.out.println("Server is Stopped.");
        } catch (RemoteException | NotBoundException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void main(String are[]) {

       // getInstance();
    }
      public ServerOperation getObj() {
        return obj;
    }

    public void setObj(ServerOperation obj) {
        this.obj = obj;
    }
}
