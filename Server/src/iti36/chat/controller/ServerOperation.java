/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.controller;

import iti36.chat.interfaces.ClientOperationInt;
import iti36.chat.dao.DatabaseOperationIn;
import iti36.chat.interfaces.ServerOperationIn;
import iti36.chat.interfaces.UserConstant;
import iti36.chat.pojo.User;
import iti36.chat.dao.impl.DatabaseOperation;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import iti36.chat.pojo.ContactList;

/**
 *
 * @author adel
 */
public class ServerOperation extends UnicastRemoteObject implements ServerOperationIn {

    DatabaseOperationIn databaseOperation;
    ClientOperationInt client;
    Map<String, ClientOperationInt> onlineUser = new HashMap<>();

    public ServerOperation() throws RemoteException {

        databaseOperation = DatabaseOperation.getInstance();

    }

    @Override
    public User login(User user) throws RemoteException {
        User newUser = databaseOperation.logIn(user);
        if (newUser == null) {
            return newUser;
        } else if (onlineUser.get(newUser.getEmail()) != null) {
            newUser.setStatus(UserConstant.INVALID);
        }

        return newUser;
    }

    @Override
    public boolean logOut() throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean regestration(User user) throws RemoteException {
        return databaseOperation.signUp(user);

    }

    @Override
    public List<User> getOnLineUsers() throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean changeUserStatus(User user) {
        boolean result = false;
        try {
            result = databaseOperation.changeStatus(user);
            ClientOperationInt sendNotificationObj;
            for (int i = 0; i < user.getContact().size(); i++) {
                sendNotificationObj = onlineUser.get(user.getContact().get(i).getEmail());
                if (sendNotificationObj != null) {
                    sendNotificationObj.sendNotification(user.getEmail(), user.getStatus());
                }
            }
            if (user.getStatus().equals(UserConstant.OFFLINE)) {
                onlineUser.remove(user.getEmail());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean changeUserMode(User user) {
        return false;
    }

    @Override
    public boolean acceptRequest(String from, String friendEmail) {
        try {
            databaseOperation.acceptFriendRequest(from, friendEmail);
            User user = databaseOperation.getUserInfo(from);
            if (onlineUser.get(from) != null) {
                onlineUser.get(from).updateUserInfo(user);
            }
            user = databaseOperation.getUserInfo(friendEmail);
            if (onlineUser.get(friendEmail) != null) {
                onlineUser.get(friendEmail).updateUserInfo(user);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean removeFriend(String user, String friendEmail) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean sendFriendRequest(String from, String to) {
        try {
            if (onlineUser.get(to) != null) {
                onlineUser.get(to).sendFriendRequest(from, to);
                return true;
            } else {
                return databaseOperation.sendFriendRequest(from, to);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean blockFriend(String user, String friendEmail) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean unBlockFriend(String user, String friendEmail) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<User> getAllUsers() {
        try {
            return databaseOperation.getAllUsers();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public void Register(ClientOperationInt client, User user) throws RemoteException {
        onlineUser.put(user.getEmail(), client);
    }

    @Override
    public ClientOperationInt sessionRegistration(ContactList client) throws RemoteException {
        return onlineUser.get(client.getEmail());
    }

    @Override
    public void sessionRegistrationConference(List<ContactList> contact, String message, String senderEmail) throws RemoteException {
        for (int i = 0; i < contact.size(); i++) {
            onlineUser.get(contact.get(i).getEmail()).sendMessage(contact, message, senderEmail);
        }
    }

    @Override
    public void sendAnnouncement(String message) throws RemoteException {
        Iterator it = onlineUser.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            ClientOperationInt send = (ClientOperationInt) pair.getValue();
            send.reciveAnnouncementMessage(message);
        }
    }
}
