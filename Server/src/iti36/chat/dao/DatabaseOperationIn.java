package iti36.chat.dao;

import iti36.chat.pojo.User;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author adel
 */
public interface DatabaseOperationIn {

    public List<User> getAllUsers();

    public boolean updateUserStatus();

    public List<User> getOffLineUsers();

    public List<User> getOnLineUsers();

    public User logIn(User user);
    
    public boolean signUp(User user);

    public boolean sendFriendRequest(String email, String frienEmail);

    public boolean acceptFriendRequest(String email, String frienEmail);

    public boolean blockFriendRequest(String email, String frienEmail);

    public void forgetPassword(String email);

    public boolean changeStatus(User user);//"busy - online -  invisble - offline"

    public boolean changeMode(User user);
    
    public int getMaleNumber();
    
    public int getFemaleNumber();
    
    public int getOnline();
    
    public int getOffline();
    
    public int getBusy();
    
    public int getAway();
    
    public User getUserInfo(String email);
    
    public boolean updateUser(User user);
    
   
}
