/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.dao.impl;

import iti36.chat.interfaces.Constatnt;
import iti36.chat.interfaces.UserConstant;
import iti36.chat.dao.DatabaseOperationIn;
import iti36.chat.dao.util.DbOpenConnection;
import java.sql.Date;
import iti36.chat.pojo.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import iti36.chat.pojo.ContactList;
import iti36.chat.pojo.FriendRequest;

/**
 *
 * @author adel
 */
public abstract class DatabaseOperation implements DatabaseOperationIn, Constatnt {

    private static DatabaseOperation instance;
    private final String GET_ALL_USERS = "select * from users";
    private final String INSERT_FRIENDREQUEST = "INSERT INTO friendrequest (emailsender, emailreciver) VALUES (?,?)";
    private final String SIGN_UP = "INSERT INTO users"
            + " (firstName,LastName,email, password, modes , Gender, status ,lastSeen ,\n"
            + "showLastSeen,country,city ,creationDate,facebookurl) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private final String GET_CONTACT_LIST = "SELECT FRIENDEMAIL from contactList where USERE_EMAIL = ?";
    private final String GET_BLOCKED_LIST = "SELECT friend_email_block from blockRequest where usere_email_block = ?";
    private final String GET_FRIEND_REQUEST_LIST = "SELECT emailSender  from friendRequest where emailReciver = ?";
    private final String GET_USER_DETAIL = "select * from users where email = ?";
    private final String CHANGE_MODE = "update users set MODES =? where email =?";
    private final String CHANGE_STATUS = "update users set STATUS =? where email =?";
    private final String GET_ONLINE_USERS = "select * from users where STATUS not in ('offline')";
    private final String GET_OFFLINE_USERS = "select * from users where STATUS ='offline'";
    private final String LOG_IN = "select * from users where email = ? and password = ?";
    private final String GET_FEMALES = "select count (gender) from users where gender='female'";
    private final String GET_MALES = "select count (gender) from users where gender='male'";
    private final String GET_ONLINE = "select count (status) from users where status='online'";
    private final String GET_OFFLINE = "select count (status) from users where status='offline'";
    private final String GET_BUSY = "select count (status) from users where status='busy'";
    private final String GET_AWAY = "select count (status) from users where status='away'";
    private final String USER_INFO = "select * from users where email = ?";
    private final String UPDATE_INFO = "update Users set firstname=?,lastname=?,password=?,gender=?,country=?,city=? where email=?";

    private DatabaseOperation() {

    }

    public static synchronized DatabaseOperation getInstance() {
        if (instance == null) {
            instance = new DatabaseOperation() {
            };
        }
        return instance;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> user = null;
        try {
            System.out.println("database.DatabaseOperation.getAllUsers()");
            user = new ArrayList<>();
            ResultSet result = DbOpenConnection.getInstance().getStatement().executeQuery(GET_ALL_USERS);
            return getUsers(result);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return user;
    }

    @Override
    public boolean changeMode(User user) {
        int result = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(CHANGE_MODE);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, user.getMode());
            DbOpenConnection.getInstance().getPreparedStatement().setString(2, user.getEmail());
            result = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (result != 0) {
            return true;
        } else {
            return false;
        }
    }

    private boolean validateNewUser() {

        return false;
    }

    @Override
    public boolean updateUserStatus() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<User> getOffLineUsers() {
        List<User> users = null;
        System.out.println("database.DatabaseOperation.getofflineUsers()");
        try {
            users = new ArrayList<>();
            ResultSet result = DbOpenConnection.getInstance().getStatement().executeQuery(GET_OFFLINE_USERS);
            return getUsers(result);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return users;
    }

    @Override
    public List<User> getOnLineUsers() {
        List<User> user = null;
        System.out.println("database.DatabaseOperation.getAllUsers()");
        try {
            user = new ArrayList<>();
            String online = "select * from users where STATUS not in ('offline')";
            ResultSet result = DbOpenConnection.getInstance().getStatement().executeQuery(online);
            return getUsers(result);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return user;
    }

    @Override
    public User logIn(User user) {
        int rowNo = 0;
        ResultSet result = null;
        try {
            DbOpenConnection.getInstance().setPreparedStatement("SELECT COUNT(*) FROM  users where email = ? and password = ?");
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, user.getEmail());
            DbOpenConnection.getInstance().getPreparedStatement().setString(2, user.getPassword());
            result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                rowNo = result.getInt(1);
            }
            System.out.println("nowwwwwwwwwwwwwwwww" + rowNo);
            if (rowNo == 1) {
                DbOpenConnection.getInstance().setPreparedStatement(LOG_IN);
                DbOpenConnection.getInstance().getPreparedStatement().setString(1, user.getEmail());
                DbOpenConnection.getInstance().getPreparedStatement().setString(2, user.getPassword());
                result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
                user = getUser(result);
                user.setStatus(UserConstant.ONLINE);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (rowNo == 0) {
            return null;
        } else {
            return user;
        }
    }

    @Override
    public boolean signUp(User user) {
        int result = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(SIGN_UP);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, user.getFirstName());
            DbOpenConnection.getInstance().getPreparedStatement().setString(2, user.getLastName());
            DbOpenConnection.getInstance().getPreparedStatement().setString(3, user.getEmail());
            DbOpenConnection.getInstance().getPreparedStatement().setString(4, user.getPassword());
            DbOpenConnection.getInstance().getPreparedStatement().setString(5, user.getMode());
            DbOpenConnection.getInstance().getPreparedStatement().setString(6, user.getGender());
            DbOpenConnection.getInstance().getPreparedStatement().setString(7, user.getStatus());
            DbOpenConnection.getInstance().getPreparedStatement().setDate(8, new Date(user.getLastSeen().getTime()));
            DbOpenConnection.getInstance().getPreparedStatement().setString(9, String.valueOf(user.isShowLastSeen()));
            DbOpenConnection.getInstance().getPreparedStatement().setString(10, user.getCountry());
            DbOpenConnection.getInstance().getPreparedStatement().setString(11, user.getCity());
            DbOpenConnection.getInstance().getPreparedStatement().setString(12, "");
            DbOpenConnection.getInstance().getPreparedStatement().setString(13, user.getUrl());
            result = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (result == 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean sendFriendRequest(String email, String friendEmail) {
        boolean check = true;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(INSERT_FRIENDREQUEST);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, email);
            DbOpenConnection.getInstance().getPreparedStatement().setString(2, friendEmail);
            DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("can not insert requist");
            check = false;
        }
        return check;
    }

    @Override
    public boolean acceptFriendRequest(String email, String friendEmail) {      // email belongs to who accepts
        boolean check = true;
        try {
            String accept = "insert into contactList (USERE_EMAIL,FRIENDEMAIL)values('";
            accept += email + "','" + friendEmail + "')";
            DbOpenConnection.getInstance().getStatement().executeQuery(accept);

            accept = "insert into contactList (USERE_EMAIL,FRIENDEMAIL)values('";
            accept += friendEmail + "','" + email + "')";
            DbOpenConnection.getInstance().getStatement().executeQuery(accept);

            accept = "DELETE FROM friendrequest WHERE emailsender='";
            accept += friendEmail + "' AND emailReciver='" + email + "'";
            DbOpenConnection.getInstance().getStatement().executeQuery(accept);

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("can not insert acception");
            check = false;
        }
        return check;
    }

    @Override
    public boolean blockFriendRequest(String email, String friendEmail) {

        boolean check = true;

        String block = "insert into blockrequest (email,friendemail)values('";
        block += email + "','" + friendEmail + "')";
        try {
            DbOpenConnection.getInstance().getStatement().executeQuery(block);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
            check = false;
        }
        return check;
    }

    @Override
    public void forgetPassword(String email) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean changeStatus(User user) {
        boolean check = true;
        String status = "update users set status='";
        status += user.getStatus() + "' where email ='" + user.getEmail() + "'";

        try {
            DbOpenConnection.getInstance().getStatement().executeQuery(status);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);

            check = false;
        }
        return check;
    }

    private List<User> getUsers(ResultSet result) {
        List users = new ArrayList<>();
        User user;
        try {
            while (result.next()) {
                user = new User();
                user.setFirstName(result.getString(FIRST_NAME));
                user.setLastName(result.getString(LAST_NAME));
                user.setEmail(result.getString(EMAIL));
                user.setPassword(result.getString(PASSWORD));
                user.setGender(result.getString(GENDER));
                user.setStatus(result.getString(STATUS));
//                if (result.getString(SHOW_LAST_SEEN).equalsIgnoreCase("true")) {
//                    user.setShowLastSeen(true);
//                } else {
//                    user.setShowLastSeen(false);
//                }
                user.setUrl(result.getString(FACEBOOK_URL));
                user.setCountry(result.getString(COUNTRY));
                user.setCity(result.getString(CITY));
                users.add(user);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return users;
    }

    private User getBlocked(User user) {
        try {
            DbOpenConnection.getInstance().setPreparedStatement(GET_BLOCKED_LIST);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, user.getEmail());
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                getContactDetail(user, result.getString(EMAIL));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    private User getContactList(User user) {
        try {
            DbOpenConnection.getInstance().setPreparedStatement(GET_CONTACT_LIST);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, user.getEmail());
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                getContactDetail(user, result.getString(FRIEND_EMAIL));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    private User getFriendRequestList(User user) {
        try {
            DbOpenConnection.getInstance().setPreparedStatement(GET_FRIEND_REQUEST_LIST);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, user.getEmail());
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                FriendRequest obj = new FriendRequest();
                obj.setEmailSender(result.getString(EMAIL_SENDER));
                user.addFriendRequest(obj);

            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    private void getContactDetail(User user, String email) {
        try {
            ContactList contact = new ContactList();
            DbOpenConnection.getInstance().setPreparedStatement(GET_USER_DETAIL);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, email);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            if (result.next()) {
                contact.setFirstName(result.getString(FIRST_NAME));
                contact.setLastName(result.getString(LAST_NAME));
                contact.setEmail(result.getString(EMAIL));
                contact.setGender(result.getString(GENDER));
                contact.setStatus(result.getString(STATUS));
//            if (result.getString(SHOW_LAST_SEEN).equalsIgnoreCase("true")) {
//                contact.setShowLastSeen(true);
//            } else {
//                contact.setShowLastSeen(false);
//            }
                contact.setUrl(result.getString(FACEBOOK_URL));
                contact.setCountry(result.getString(COUNTRY));
                contact.setCity(result.getString(CITY));
                user.addContact(contact);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public int getMaleNumber() {
        int num = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(GET_MALES);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {

                num = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return num;
    }

    public int getFemaleNumber() {
        int num = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(GET_FEMALES);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {

                num = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return num;
    }

    @Override
    public int getOnline() {
        int num = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(GET_ONLINE);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {

                num = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return num;
    }

    @Override
    public int getOffline() {
        int num = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(GET_OFFLINE);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {

                num = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return num;
    }

    @Override
    public int getBusy() {
        int num = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(GET_BUSY);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {

                num = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return num;
    }

    @Override
    public int getAway() {
        int num = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(GET_AWAY);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {

                num = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return num;
    }

    @Override
    public User getUserInfo(String email) {

        User user = new User();
        try {

            DbOpenConnection.getInstance().setPreparedStatement(USER_INFO);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, email);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();

            user = getUser(result);

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    @Override
    public boolean updateUser(User user) {
        boolean check = true;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(UPDATE_INFO);

            DbOpenConnection.getInstance().getPreparedStatement().setString(1, user.getFirstName());
            DbOpenConnection.getInstance().getPreparedStatement().setString(2, user.getLastName());
            DbOpenConnection.getInstance().getPreparedStatement().setString(3, user.getPassword());
            DbOpenConnection.getInstance().getPreparedStatement().setString(4, user.getGender());
            DbOpenConnection.getInstance().getPreparedStatement().setString(5, user.getCountry());
            DbOpenConnection.getInstance().getPreparedStatement().setString(6, user.getCity());
            DbOpenConnection.getInstance().getPreparedStatement().setString(7, user.getEmail());
            DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
            check = false;
        }

        return check;
    }

    private User getUser(ResultSet result) {

        User user = new User();
        try {
            while (result.next()) {

                user.setFirstName(result.getString(FIRST_NAME));
                user.setLastName(result.getString(LAST_NAME));
                user.setEmail(result.getString(EMAIL));
                user.setPassword(result.getString(PASSWORD));
                user.setGender(result.getString(GENDER));
                user.setStatus(result.getString(STATUS));
//                if (result.getString(SHOW_LAST_SEEN).equalsIgnoreCase("true")) {
//                    user.setShowLastSeen(true);
//                } else {
//                    user.setShowLastSeen(false);
//                }
                user.setUrl(result.getString(FACEBOOK_URL));
                user.setCountry(result.getString(COUNTRY));
                user.setCity(result.getString(CITY));
                getContactList(user);
                getBlocked(user);
                getFriendRequestList(user);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

}
