/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.dao.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleDriver;

/**
 *
 * @author adel
 */
public class DbOpenConnection {

    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;
    private static DbOpenConnection instance;

    public static synchronized DbOpenConnection getInstance() {
        if (instance == null) {
            instance = new DbOpenConnection();
        }
        return instance;
    }

    private DbOpenConnection() {
        openconnection();
    }

    private void openconnection() {
        try {
            DriverManager.registerDriver(new OracleDriver());
            connection = DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1:1521:xe", "Adel", "ITI");
            statement = connection.createStatement();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            connection.close();
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(DbOpenConnection.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }
      public PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }

    public void setPreparedStatement(String sql) throws SQLException {
        preparedStatement = connection.prepareStatement(sql);
    }
}
