/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import iti36.chat.pojo.ContactList;
import iti36.chat.pojo.User;

/**
 *
 * @author adel
 */
public interface ClientOperationInt extends Remote {

    public User clientLogin(User user) throws RemoteException;

    public boolean clientlogOut() throws RemoteException;

    public boolean clientRegestration() throws RemoteException;

    public List<User> getOnLineUsers() throws RemoteException;

    public boolean changeUserStatus(User user) throws RemoteException;

    public boolean changeUserMode(User user) throws RemoteException;

    public boolean acceptRequest(String user, String friendEmail) throws RemoteException;

    public boolean removeFriend(String user, String friendEmail) throws RemoteException;

    public void sendFriendRequest(String user, String friendEmail) throws RemoteException;

    public boolean blockFriend(String user, String friendEmail) throws RemoteException;

    public boolean unBlockFriend(String user, String friendEmail) throws RemoteException;

    public void sendMessage(List<ContactList> contact, String message, String emailSender) throws RemoteException;

    public void sendMessage(String message, String emailSender) throws RemoteException;

    public void sendNotification(String email, String staus) throws RemoteException;

    public void reciveAnnouncementMessage(String message) throws RemoteException;

    public void updateUserInfo(User user) throws RemoteException;

    public boolean sendFileAck(ContactList contact) throws RemoteException;

}
