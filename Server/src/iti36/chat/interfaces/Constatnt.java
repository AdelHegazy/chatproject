/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.interfaces;

/**
 *
 * @author adel
 */
public interface Constatnt {
    public static int PORT = 2002;
    public static String REGISTRY_NAME = "ChatService";
    public final String FIRST_NAME = "firstName";
    public final String LAST_NAME = "LastName";
    public final String EMAIL = "email";
    public final String PASSWORD = "password";
    public final String MODE = "modes";
    public final String GENDER = "Gender";
    public final String STATUS = "status";
    public final String LAST_SEEN = "lastSeen";
    public final String SHOW_LAST_SEEN = "showLastSeen";
    public final String COUNTRY = "country";
    public final String CITY = "city";
    public final String CREATION_DATE = "creationDate";
    public final String FACEBOOK_URL = "facebookurl";
    public final String FRIEND_EMAIL = "FRIENDEMAIL";
    public final String EMAIL_SENDER= "emailSender";
}
