/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.pojo;

import java.io.Serializable;

/**
 *
 * @author MANAR ADEL
 */
public class Announce implements Serializable
{
    private long AID ;
    private String announcement;
    
     public long getAID() 
    {
        return AID;
    }

    public void setAID(long AID) 
    {
        this.AID = AID;
    }
    
     public String getAnnouncement() 
    {
        return announcement;
    }

    public void setAnnouncement(String announcement) 
    {
        this.announcement = announcement;
    }
    
    
}
