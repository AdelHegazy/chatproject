/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.pojo;

import java.io.Serializable;

/**
 *
 * @author MANAR ADEL
 */
public class FriendRequest implements Serializable
{
    private String emailSender;
    private String emailReciver;
    
    
    public String getEmailSender() 
    {
        return emailSender;
    }

    public void setEmailSender(String emailSender) 
    {
        this.emailSender = emailSender;
    }
    
    public String getEmailReciver() 
    {
        return emailReciver;
    }

    public void setEmailReciver(String emailReciver) 
    {
        this.emailReciver = emailReciver;
    }
    
    
    
    //emailSender= new User().getEmail();
    

    
}
