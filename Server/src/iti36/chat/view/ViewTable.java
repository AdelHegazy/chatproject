/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.chat.view;
 
import iti36.chat.dao.DatabaseOperationIn;
import iti36.chat.dao.impl.DatabaseOperation;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.sql.ResultSet;
import javax.swing.*;
import iti36.chat.pojo.User;
import java.util.*;

/**
 *
 * @author Hosam
 */
public class ViewTable extends JPanel{
    
    /**
     * @param args the command line arguments
     */
    
    private boolean DEBUG = false;
     
    public ViewTable(){
        
        super(new GridLayout(1,0));
        
       Vector v=new Vector();
       
       v.add("E-mail");
       v.add("FName");
       v.add("LName");
       v.add("Password");
       v.add("Gender");
       v.add("Status");
       
       //v.add("LastSeen");
       
      
       Vector v2=new Vector();
       List<User>all=DatabaseOperation.getInstance().getAllUsers();
      
       for(User u :all)
       {
           Vector<String> vstring = new Vector<String>();
           vstring.add(u.getEmail());
           vstring.add(u.getFirstName());
           vstring.add(u.getLastName());
           vstring.add(u.getPassword());
           vstring.add(u.getGender());
           vstring.add(u.getStatus());
           //vstring.add(u.getLastSeen());
           
           v2.add(vstring);
       }
       
       final javax.swing.JTable table=new javax.swing.JTable(); 
       javax.swing.table.DefaultTableModel mod = new javax.swing.table.DefaultTableModel(v2, v);
          table.setModel(mod);
          
          //table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        table.setFillsViewportHeight(true);
        
        if (DEBUG) {
            table.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                    printDebugData(table);
                }
            });
        }
        
        
        JScrollPane scrollPane = new JScrollPane(table);
 
         add(scrollPane);
         
         
    }
    
    private void printDebugData(javax.swing.JTable table) {
        int numRows = table.getRowCount();
        int numCols = table.getColumnCount();
        javax.swing.table.TableModel model = table.getModel();
 
        System.out.println("Value of data: ");
        for (int i=0; i < numRows; i++) {
            System.out.print("    row " + i + ":");
            for (int j=0; j < numCols; j++) {
                System.out.print("  " + model.getValueAt(i, j));
            }
            System.out.println();
        }
        System.out.println("--------------------------");
    }
    
    private static void createAndShowGUI() {
       JFrame frame = new JFrame("ALL Users");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
 
        //Create and set up the content pane.
        ViewTable newContentPane = new ViewTable();
        newContentPane.setOpaque(true); 
        frame.setContentPane(newContentPane);
        
        //Display the window.
        frame.pack();
        frame.setVisible(true);
        
    }
    
    public static void view(){
        // TODO code application logic here
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
    }
        });
    }
}    
                
